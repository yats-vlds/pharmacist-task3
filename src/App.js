import React, {useEffect, useState} from "react";
import answerA from "./SVG/answerA.svg"
import answerA_After from "./SVG/answerA_After.svg"
import answerB from "./SVG/answerB.svg"
import answerB_After from "./SVG/answerB_After.svg"
import answerC from "./SVG/answerC.svg"
import answerC_After from "./SVG/answerC_After.svg"
import question from "./SVG/question.svg"
import clock from "./SVG/clock.svg"
import train1 from "./SVG/train1.svg"
import train2 from "./SVG/train2.svg"
import train3 from "./SVG/train3.svg"
import train4 from "./SVG/train4.svg"
import upstairsRoad1 from "./SVG/upstairsRoad1.svg"
import upstairsRoad2 from "./SVG/updstarsRoads2.svg"
import upstairsRoad3 from "./SVG/upstairsRoad3.svg"
import './App.css';

function App() {
    const TIMER = {
        PAUSED: "PAUSED",
        TICKING: "TICKING"
    };
    const [showAnswer, setShowAnswer] = useState(false)
    const [checkEl, setCheckEl] = useState('')
    const [text, setText] = useState(60);
    const [current, setCurrent] = useState(null);
    const [timerState, setTimerState] = useState(TIMER.PAUSED);

    function isTicking() {
        return timerState === TIMER.TICKING;
    }

    function pause() {
        setTimerState(TIMER.PAUSED);
    }

    function handleStart() {
        if (isTicking()) {
            return;
        }

        if (!current) {
            setCurrent(Number(text));
        }
        setShowAnswer(true)

        setTimerState(TIMER.TICKING);
    }

    function handlePause() {
        pause();
    }

    useEffect(() => {
        let intervalId;

        function reset() {
            pause();
            setCurrent("0");
        }

        function pauseTimer() {
            clearInterval(intervalId);
        }

        function tick() {
            setCurrent((current) => {
                if (current === 0) {
                    reset();
                }

                return current - 1;
            });
        }

        switch (timerState) {
            case TIMER.PAUSED:
                break;
            case TIMER.TICKING:
                intervalId = setInterval(tick, 1000);
                break;
            default:
                break;
        }

        return pauseTimer;
    }, [timerState]);

    return (
        <div className="container-fluid">
            <div className="row align-items-start">
                <div className="col-4">
                    <h1 className="clock-text">{current === null ? "60" : current}</h1>
                    <button onClick={handleStart} disabled={isTicking()} className="start">
                        Start
                    </button>
                    <button onClick={handlePause} disabled={!isTicking()} className="pause">
                        Pause
                    </button>
                    <button className="reset" onClick={() => setCurrent(null)}>RESET</button>
                </div>
                <div className="col-8">
                    <h1 className="question">Text</h1>
                </div>
                <div className="col">
                    {showAnswer ? <>
                        <div className="answer">
                            <img src={checkEl === 'answerA' ? answerA_After : answerA} alt="answerA" className="answer__img" onClick={() => setCheckEl('answerA')} />
                            <img src={checkEl === 'answerB' ? answerB_After : answerB} alt="answerB" className="answer__img" onClick={() => setCheckEl('answerB')} />
                            <img src={checkEl === 'answerC' ? answerC_After : answerC} alt="answerC" className="answer__img" onClick={() => setCheckEl('answerC')} />
                        </div>
                        <div className="answerText">
                            <h3 onClick={() => setCheckEl('answerA')}>Hello</h3>
                            <h3 onClick={() => setCheckEl('answerB')}>Hello</h3>
                            <h3 onClick={() => setCheckEl('answerC')}>Hello</h3>
                        </div>
                    </> : <div className="answerWithoutAny"></div>}
                </div>
            </div>
            <div className="row">
               <div className="col">
                   <div className="train">
                       <div className="line">
                           <img src={train1} alt="train1" className="train1"/>
                       </div>
                       {/*<div className="line">*/}
                       {/*    <img src={train2} alt="train2" className="train2"/>*/}
                       {/*</div>*/}
                       {/*<div className="line">*/}
                       {/*    <img src={train3} alt="train3" className="train3"/>*/}
                       {/*</div>*/}
                       {/*<div className="line">*/}
                       {/*    <img src={train4} alt="train4" className="train4"/>*/}
                       {/*</div>*/}
                   </div>
               </div>
            </div>
        </div>
    );
}

export default App;
